package main

import "fmt"

var base int = 10
var unityRoot int

func calUnityRoot(num int) int {
	fmt.Printf("Calculate Unity Root for %v\n", num)
	if num <= 9 {
		return num + unityRoot
	} else {
		remainder := num % base
		num -= remainder
		num = num / base
		unityRoot += remainder
		fmt.Printf("Remainder %v\tNumber: %v\tUnityRoot %v\n", remainder, num, unityRoot)
		calUnityRoot(num)
		if unityRoot >= 10 {
			unityRoot = calUnityRoot(unityRoot)
		}

		return unityRoot
	}
}

func user_input() int {
	var number int
	fmt.Println("\nEnter number for Unity root calculation:\t")
	fmt.Scanln(&number)
	return number
}

func main() {
	fmt.Println("\nUnity Root Calculator\n")
	unum := user_input()
	uroot := calUnityRoot(unum)
	fmt.Printf("Unity Root is %v", uroot)
	//	fmt.Printf("User gave %v", unum)
}
