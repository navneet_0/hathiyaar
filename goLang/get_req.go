package main

import (
	"fmt"
	//"io/ioutil"
	"log"
	"net/http"
)

var targetURL string = "https://jsonplaceholder.typicode.com/posts"

func main() {

	colorReset := "\033[0m"
	colorGreen := "\033[32m"
	colorBlue := "\033[34m"
	colorRed := "\033[31m"

	fmt.Println("Go GET Request\n")
	httpResponse, err := http.Get(targetURL)
	if err != nil {
		log.Fatalln(string(colorRed), err)
	}
	defer httpResponse.Body.Close()
	/*
		resp_body, err := ioutil.ReadAll(http_response.Body)
		if err != nil {
			log.Fatalln(err)
		} */
	//http_response.Header is a map
	//log.Printf(string(resp_body))
	// fmt.Printf("%T", httpResponse.Status)
	// fmt.Println("\n\n")
	log.Println(httpResponse.Status)
	log.Println(httpResponse.StatusCode)
	for k, v := range httpResponse.Header {
		fmt.Println(string(colorGreen), k, ":", string(colorReset), string(colorBlue), v, string(colorReset))
	}
}
