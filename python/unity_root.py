
def cal_unity_root(input, remainder):
    
    if input == 0:
        if remainder >=10:
            return cal_unity_root(remainder, 0)
        return remainder
    else:
        r = input%10
        return cal_unity_root(int(input/10), remainder+r)


def get_user_input():
    user_input = input("Input\t: ")
    return int(user_input)

if __name__ == '__main__':
    user_input = get_user_input()
    unity_root = cal_unity_root(input=user_input, remainder=0)
    print(f"Unity root for {user_input} is  = {unity_root}")
