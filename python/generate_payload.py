from random import *
from termcolor import cprint
from pyfiglet import figlet_format
import subprocess
payload_file = 'payload.txt'

FAIL = '\033[91m'
ENDC = '\033[0m'



def print_banner():
    cprint(figlet_format('payload generator'))

def get_user_choice():
    print("\n=======================================================")
    print("1. ASCII Payload\t2. Interger Payload\t0. Quit")
    print("=======================================================")
    user_choice = input("\nEnter Choice\t: ")
    return int(user_choice)


def crash_payload():
    print("\nGenerating payload for input crash")
    payload_length = int(input("Enter payload length:\t"))
    print(f"Opening file {payload_file} for generating payload of random characters using {payload_length} random int to ascii")
    try:
        with open (payload_file,'w') as f:
            print("opened file...writing payload to file now")
            f.write('\'')
            for i in range(payload_length):
                num = randint(65,122)
                p = chr(num)
                f.write(p)
            f.write("\'")
        f.close()
    except:
        print("Cannot open file for writing payload")
    print("Payload generation completed")

def numbers_payload():
    print("\nNumbers Payload Generator\n")
    upper_limit = int(input("Enter upper limit (e.g 10,50,1000)\t:"))
    file_name = 'numbers_payload'+str(upper_limit)+'.txt'
    try:
        with open(file_name,'w') as f:
            print(f"Opened file {file_name} for writing")
            for i in range(upper_limit+1):
                f.write(str(i))
                f.write("\n")
                i+=1
    except Exception as e:
        print(f"{FAIL}Error opening file for generating payload {e}{ENDC}")
    print("Payload Generation Complete")
    return



if __name__ == "__main__":
    # print("\tPayload Generator\n")
    subprocess.call("clear")
    print_banner()
    choice = 9
    while(choice != 0):
        choice = get_user_choice()
        if choice == 1:
            crash_payload()
        elif choice == 2:
            numbers_payload()
        elif choice == 0:
            break
        else:
            print(f"{FAIL}==============={choice} is a Wrong Choice=====================\n{ENDC}")
            get_user_choice()
