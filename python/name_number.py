# ''' Convert name to number based on Chaldean System'''
import datetime
import subprocess

from termcolor import cprint
from pyfiglet import figlet_format

from unity_root import cal_unity_root

name_number = {
    'a': 1,
    'b': 2,
    'c': 3,
    'd': 4,
    'e': 5,
    'f': 8,
    'g': 3,
    'h': 5,
    'i': 1,
    'j': 1, 
    'k': 2,
    'l': 3, 
    'm': 5,
    'n': 5,
    'o': 7, 
    'p': 8,
    'q': 1,
    'r': 2,
    's': 3, 
    't': 4, 
    'u': 6,
    'v': 6,
    'w': 6,
    'x': 5, 
    'y': 1,
    'z': 7,
    ' ': 0
}

heart_number = {'a':1, 'e':5, 'i':1, 'o':7, 'u':6}

def display_banner():
    subprocess.call("clear")
    cprint(figlet_format('Number GAME'))

def calculate_personal_year(date,month):
    current_year = datetime.datetime.now().year
    number = date+month+str(current_year)
    personal_year = cal_unity_root(input=int(number), remainder=0)
    return personal_year


def calculate_material_number(name):
    total = 0
    for c in name:
        try:
            vowel_number = heart_number[c]
            # print(vowel_number, c)
        except:
            consonant_number = name_number[c]
            total += consonant_number
    if total > 9:
        total = cal_unity_root(input=total, remainder=0)
    return total


def calculate_heart_number(name):
    # print(f"Calculating heart number for {name}")
    total = 0
    for c in name:
        try:
            char_number = heart_number[c]
            total += char_number
            # print(total, char_number)
        except:
            pass
    if total > 9:
        total = cal_unity_root(input=total, remainder=0)
    return total

def get_user_input():
    name = input("Enter name \t: ")
    dob = input("Enter date of birth DD-MM-YYYY \t: ")
    name.replace(" ","")
    # print(name)
    return name.lower(), dob


def get_total_of_name(input):
    total = 0
    for c in input:
        char_number = name_number[c]
        total += char_number
        # print(c)
    return total



if __name__ == '__main__':
    user_name, user_dob = get_user_input()
    date_numbers = user_dob.split('-')
    date = date_numbers[0]
    month = date_numbers[1]
    birth_year = date_numbers[2]
    full_date = date+month+birth_year
    pyschic_number = int(date)
    if pyschic_number > 9:
        pyschic_number = cal_unity_root(input=pyschic_number, remainder=0)

    destiny_number = cal_unity_root(input=int(full_date), remainder=0)

    birth_year = cal_unity_root(input=int(birth_year), remainder=0)

    number_name = get_total_of_name(user_name)
    if number_name > 9:
        number_name = cal_unity_root(input=number_name, remainder=0)

    
    display_banner()
    print(f"\nName number for {user_name} as per Chaldian System is {number_name} ")
    print(f"Pyschic Number : {pyschic_number}")
    print(f"Destiny Number : {destiny_number}")
    print(f"Birth Year Number: {birth_year}")
    print(f"Heart Number : {calculate_heart_number(user_name)}")
    print(f"Material Number : {calculate_material_number(user_name)}")
    print(f"Personal Year : {calculate_personal_year(date, month)}\n")