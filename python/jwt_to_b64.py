#To Convert JWT token to readable form
import base64
import json
from termcolor import cprint
from pyfiglet import figlet_format
import subprocess

DEFAULT_ENCODING = 'UTF-8'

ENDC = '\033[0m'
colorReset = "\033[0m"
REDC = "\033[31m"
YELLOWC = "\033[33m"
colorGreen = "\033[32m"
colorBlue = "\033[34m"
colorPurple = "\033[35m"
colorCyan = "\033[36m"
colorWhite = "\033[37m"

HEADER = '\033[95m'
OKBLUE = '\033[94m'
OKGREEN = '\033[92m'
WARNING = '\033[93m'
FAIL = '\033[91m'
BOLD = '\033[1m'
UNDERLINE = '\033[4m'

def display_banner():
    subprocess.call("clear")
    cprint(figlet_format('JWT Decoder'))

def b64_decode(encoded_str):
    return base64.standard_b64decode(bytes(encoded_str,encoding=DEFAULT_ENCODING)+b'==')

def jwt_to_b64_readable():
    token = input("Enter the token\n\t:")
    token_parts = token.split('.')
    token_header = b64_decode(token_parts[0])
    token_body = b64_decode(token_parts[1])
    # token_signature = b64_decode(token_parts[2])
    token_signature = token_parts[2]
    header_json = json.loads(token_header.decode(DEFAULT_ENCODING))
    body_json = json.loads(token_body.decode(DEFAULT_ENCODING))
    print(f"\n{OKBLUE}Header:\n\t{json.dumps( header_json, indent=3)}{ENDC}")
    print(f"{OKGREEN}Claim:\n\t{json.dumps( body_json, indent=3)}{ENDC}")
    print(f"{YELLOWC}Signature:\t{token_signature}\n\n{ENDC}")

if __name__ == '__main__':
    display_banner()
    jwt_to_b64_readable()
